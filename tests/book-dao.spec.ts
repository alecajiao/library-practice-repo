import { client } from "../src/connection";
import { BookDaoPostgres } from "../src/daos/book-dao-postgres";
import { BookDaoTextFile } from "../src/daos/book-dao-textfile-impl";
import { BookDAO } from "../src/daos/book-daos";
import { Book } from "../src/entities";

const bookDAO:BookDAO = new BookDaoPostgres();

//Any entity obejct that has not been saves somewhere should have the id of 0
const testBook:Book = new Book(
    0, 
    "The Hobbit", 
    "Tolkien", 
    true, 
    1, 
    0);

    test("Create a book", async () =>{
        const result:Book = await bookDAO.createBook(testBook);
        //An entiy that saved should have a non-zero id
        expect(result.bookId).not.toBe(0); 
    });

    //An integration test requires that two or more functions you wrote pass
    test("Get book by Id", async ()=>{
        let book:Book = new Book(0, "Dracula", "Bram Stoker", true, 1, 0);
        book = await bookDAO.createBook(book);

        let retrievedBook:Book = await bookDAO.getBookById(book.bookId);

        expect(retrievedBook.title).toBe(book.title);
    });

    test("Get all books", async()=>{
        let book1:Book = new Book(0, "Sapiens", "Yuval", true, 0 , 0);
        let book2:Book = new Book(0, "1984", "Gorge Orwell", true, 1 , 1);
        let book3:Book = new Book(0, "Paradox of Choice", "Bery Schwartz", true, 0 , 0);

        await bookDAO.createBook(book1);
        await bookDAO.createBook(book2);
        await bookDAO.createBook(book3);

        const books:Book[] = await bookDAO.getAllBook();
        expect(books.length).toBeGreaterThanOrEqual(3);
    });

    test("", async ()=> {
        let book:Book = new Book(0,"We have always lived in the castle", "Shirley Jackson", true, 1, 0);
        book = await bookDAO.createBook(book);

        book.quality = 4;
        book = await bookDAO.updateBook(book);

        expect(book.quality).toBe(4);
    });

    test("delete book by id", async ()=>{
        let book:Book = new Book(0,"frankenstien", "Mary Shelley", true, 1, 0);
        book = await bookDAO.createBook(book);

        const result:boolean = await bookDAO.deleteBookById(book.bookId);
        expect(result).toBeTruthy();
    });

    afterAll(async () => {
        client.end(); // close connection
    });


