import {client} from '../src/connection';

// client is the main obejct we will use to make queries

test("Should create a connection", async () => {
    const result = await client.query('select * from book');
    console.log(result);
});

afterAll(()=>{client.end();});