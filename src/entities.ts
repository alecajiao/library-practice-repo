// An entity is a classs that stores info that will ultimately be presisted somewhere
//Usually should alwasy have one field in them that is a unique identifies, ID

export class Book {
    constructor(
        public bookId:number,
        public title:string,
        public author:string,
        public isAvailable:boolean,
        public quality:number,
        public returnDate: number){}
}