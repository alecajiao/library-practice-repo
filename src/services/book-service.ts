import {Book} from "../entities";

export default interface BookService{

    registerBook(book:Book):Promise<Book>;

    retriveAllBooks():Promise<Book[]>;

    retriveBookById(bookId:number):Promise<Book>;

    checkOutBookById(bookId:number):Promise<Book>;

    checkInBookById(bookId:number):Promise<boolean>;

    searchByTitle(title:string):Promise<Book[]>;

    modifyBook(book:Book):Promise<Book>;

    removeBookById(bookId:number):Promise<boolean>;

};