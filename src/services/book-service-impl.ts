import { BookDaoPostgres } from "../daos/book-dao-postgres";
import { BookDaoTextFile } from "../daos/book-dao-textfile-impl";
import { BookDAO } from "../daos/book-daos";
import { Book } from "../entities";
import BookService from "./book-service";

export class BookServiceImpl implements BookService{
    bookDAO:BookDAO = new BookDaoPostgres();

    registerBook(book: Book): Promise<Book> {
        book.returnDate = 0; //services are often sanitize inputs or set default values
        return this.bookDAO.createBook(book);
    }

    retriveAllBooks(): Promise<Book[]> {
        return this.bookDAO.getAllBook();
    }

    retriveBookById(bookId: number): Promise<Book> {
        return this.bookDAO.getBookById(bookId);
    }

    //service methods also perform business logic
    async checkOutBookById(bookId: number): Promise<Book> {
        let book:Book = await this.bookDAO.getBookById(bookId);
        book.isAvailable = false;
        book.returnDate = Date.now() + 1_209_600;
        // book = await this.bookDAO.updateBook(book);
        // return book;
        return await this.bookDAO.updateBook(book);
    }

    checkInBookById(bookId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    searchByTitle(title: string): Promise<Book[]> {
        throw new Error("Method not implemented.");
    }

    modifyBook(book: Book): Promise<Book> {
        throw new Error("Method not implemented.");
    }

    removeBookById(bookId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}