//DAO (Data Access Object)
//class resposible for presisting an entity
//A DAO should support CRUD functionality

import { Book } from "../entities";


export interface BookDAO{
    //CREATE
    createBook(book:Book):Promise<Book>;

    //READ
    getAllBook():Promise<Book[]>;
    getBookById(bookId:number):Promise<Book>;

    //UPDATE
    updateBook(book:Book):Promise<Book>;

    //DELETE
    deleteBookById(bookId:number):Promise<boolean>;

}