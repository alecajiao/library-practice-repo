import { Book } from "../entities";
import { BookDAO } from "./book-daos";
import { readFile, writeFile} from "fs/promises";
import { MissingResourceError } from "../errors";

export class BookDaoTextFile implements BookDAO{
    async createBook(book: Book): Promise<Book> {
        const fileData:Buffer = await readFile("/Users/gnz11/Documents/revature/libraryApi/books.txt");
        const textData:string = fileData.toString();
        const books:Book[] = JSON.parse(textData); 
        
        book.bookId = Math.round(Math.random() * 1000);
        
        books.push(book);

       await writeFile(
           "/Users/gnz11/Documents/revature/libraryApi/books.txt", 
            JSON.stringify(books));

        return book;
    }
    async getAllBook(): Promise<Book[]> {
        const fileData:Buffer = await readFile("/Users/gnz11/Documents/revature/libraryApi/books.txt");
        const textData:string = fileData.toString();
        const books:Book[] = JSON.parse(textData); 

        return books;
    }
    async getBookById(bookId: number): Promise<Book> {
        const fileData:Buffer = await readFile("/Users/gnz11/Documents/revature/libraryApi/books.txt");
        const textData:string = fileData.toString();
        const books:Book[] = JSON.parse(textData); 

        for(const book of books){
            if(book.bookId === bookId){
                return book;
            }
        }
        throw new MissingResourceError(`The book with ID ${bookId} could not be located.`);
    }
    async updateBook(book: Book): Promise<Book> {
        const fileData:Buffer = await readFile("/Users/gnz11/Documents/revature/libraryApi/books.txt");
        const textData:string = fileData.toString();
        const books:Book[] = JSON.parse(textData); 

        for(let i = 0; i < books.length; i++){
            if(books[i].bookId === book.bookId){
                books[i] = book;
            }
        }

        await writeFile(
            "/Users/gnz11/Documents/revature/libraryApi/books.txt", 
             JSON.stringify(books));
 
         return book;

    }
    async deleteBookById(bookId: number): Promise<boolean> {
        const fileData:Buffer = await readFile("/Users/gnz11/Documents/revature/libraryApi/books.txt");
        const textData:string = fileData.toString();
        const books:Book[] = JSON.parse(textData); 

        for(let i = 0; i < books.length; i++){
            if(books[i].bookId === bookId){
                books.splice(i);
                await writeFile(
                    "/Users/gnz11/Documents/revature/libraryApi/books.txt", 
                     JSON.stringify(books));
                return true;
            }
        }
         return false;
    }
}