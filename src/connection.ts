// import * as dotenv from 'dotenv';
require('dotenv').config({path:'/Users/gnz11/Documents/revature/library-practice-repo/app.env'})
import {Client} from 'pg';


// dotenv.config();
// console.log(process.env.DBPW);

export const client = new Client({
    user:'postgres',
    password:process.env.DBPW, // should never store passwords on code
    database:process.env.DBNAME,
    port:5432,
    host: '35.231.148.127'
});

client.connect();

