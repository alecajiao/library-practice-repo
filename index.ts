import express from 'express';
import { Book } from './src/entities';
import { MissingResourceError } from './src/errors';
import BookService from './src/services/book-service';
import { BookServiceImpl } from './src/services/book-service-impl';

const app = express();
app.use(express.json()); //middleware

const bookService:BookService = new BookServiceImpl();

app.get("/books", async (req, res) => {
    console.log("hit books");
    const books:Book[] = await bookService.retriveAllBooks();
    res.send(books);
});

app.get("/books/:id", async (req, res) => {
    try {
        const bookId = Number(req.params.id);
        const book:Book = await bookService.retriveBookById(bookId);
        res.send(book);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.post("/books", async (req, res) => {
    let book:Book = req.body;
    book = await bookService.registerBook(book);
    res.send(book);
});

app.patch("/books/:id/checkout", async (req, res) => {
    const bookId = Number(req.params.id);
    const book = await bookService.checkOutBookById(bookId);
    res.send(book);
});



app.listen(3000, ()=>{console.log("Application Started")});


